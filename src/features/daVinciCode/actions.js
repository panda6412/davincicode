export const setData = payload => ({
  type: 'SET_DATA',
  payload
})

export const addRank = rank => ({
  type: 'ADD_RANKS',
  payload: rank
})

export const resetGame = () => ({
  type: 'RESET_GAME'
})

export const resetData = () => ({
  type: 'RESET_DATA'
})