const initialState = {
  aspect: 'GAME_MENU',
  username: '',
  targetNumber: undefined,
  min: 1,
  max: 50,
  guessCount: 0,

  ranks: []
};

export const daVinciCode = (state = initialState, action) => {
  switch(action.type) {
    case 'SET_DATA':
      return {
        ...state,
        ...action.payload
      }
    case 'ADD_RANKS':
      return {
        ...state,
        ranks : [
          ...state.ranks || [],
          action.payload
        ]
      }
    case 'RESET_GAME':
      return {
        ...initialState,
        ranks: [...state.ranks || []]
      }
    case 'RESET_DATA':
      return {
        ...initialState
      }
    default:
      return state
  }
}
