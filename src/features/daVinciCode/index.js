import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setData, resetGame, addRank } from './actions';
import { ASPECT } from './consts';

const DaVinciCode = () => {
  const aspect = useSelector(state => state.daVinciCode.aspect)

  switch(aspect) {
    case ASPECT.GAME_MENU:
      return <GameMenu />
    case ASPECT.GAME:
      return <Game />
    case ASPECT.RANK:
      return <Rank />
    default:
      return <GameMenu />
  }
}

const GameMenu = () => {
  const username = useSelector(state => state.daVinciCode.username)
  const ranks = useSelector(state => state.daVinciCode.ranks)
  const dispatch = useDispatch()
  const generateTargetNumber = max => Math.floor(Math.random() * max) + 1

  const [tip, setTip] = useState()

  return (
    <div>
      <div>
        <input value={username} onChange={e => dispatch(setData({ username: e.target.value }))} />
      </div>
      {tip && <div style={{ color: 'red', marginBottom: 16 }}>{tip}</div>}
      <button disabled={!username} onClick={() => {
        if (ranks.map((rank) => rank.username).includes(username)) {
          setTip(`${username} 已存在排名了喲`)
          return
        }

        const targetNumber = generateTargetNumber(50)
        dispatch(setData({ aspect: ASPECT.GAME, targetNumber }))
      }}>遊戲開始</button>

      {ranks.length > 0 && <button onClick={() => dispatch(setData({ aspect: ASPECT.RANK }))}>查看名次</button>}
    </div>
  )
}

const Game = () => {
  const username = useSelector(state => state.daVinciCode.username)
  const targetNumber = useSelector(state => state.daVinciCode.targetNumber)
  const min = useSelector(state => state.daVinciCode.min)
  const max = useSelector(state => state.daVinciCode.max)
  const guessCount = useSelector(state => state.daVinciCode.guessCount)
  const tip = useSelector(state => state.daVinciCode.tip)
  const isGameEnd = useSelector(state => state.daVinciCode.isGameEnd)
  const dispatch = useDispatch()

  const [guessInput, setGuessInput] = useState('')

  return (
    <div>
      <div>累積次數: {guessCount}</div>
      <input disabled={isGameEnd} value={guessInput} onChange={e => setGuessInput(e.target.value)}/>
      <button disabled={isGameEnd} onClick={() => {
        let tip = ''
        let isGameEnd = false
        const nextCount = guessCount + 1

        if (Number(targetNumber) > Number(guessInput) && Number(guessInput) < Number(max)) {
          tip = `未猜中，縮限範圍到${guessInput}~${max}`
          dispatch(setData({ min: Number(guessInput) }))
        } else if (Number(targetNumber) < Number(guessInput) && Number(guessInput) > Number(min)) {
          tip = `未猜中，縮限範圍到${min}~${guessInput}`
          dispatch(setData({ max: Number(guessInput) }))
        } else if (Number(guessInput) === Number(targetNumber)) {
          tip = '恭喜您猜中數字了'
          isGameEnd = true
        }
        dispatch(setData({ isGameEnd, guessCount: nextCount, tip }))
        isGameEnd && dispatch(addRank({ username, targetNumber, guessCount: nextCount }))
      }}>猜</button>
      {tip && <div>{tip}</div>}

      <button onClick={() => dispatch(resetGame())}>返回</button>
      {isGameEnd && <button onClick={() => dispatch(resetGame())}>再玩一次</button>}
    </div>
  )
}

const Rank = () => {
  const ranks = useSelector(state => state.daVinciCode.ranks)
  const dispatch = useDispatch()
  return (
    <div>
      {ranks.length > 0 && (
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <table>
            <thead>
              <tr>
                <th>名次</th>
                <th>名字</th>
                <th>猜的次數</th>
              </tr>
            </thead>
            <tbody>
              {ranks
                .sort((a, b) => b.guessCount - a.guessCount)
                .map(({ username, guessCount }, index) => {
                  return (
                    <tr key={index}>
                      <td>{index + 1}</td>
                      <td>{username}</td>
                      <td>{guessCount}</td>
                    </tr>
                  )
                })}
            </tbody>
          </table>
        </div>
      )}
      <button onClick={() => dispatch(resetGame())}>返回</button>
    </div>
  )
}

export default DaVinciCode