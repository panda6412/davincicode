import React from 'react';
import DaVinciCode from './features/daVinciCode';
import './App.css';

function App() {
  return (
    <div className="App">
      {/* <Counter /> */}
      <DaVinciCode />
    </div>
  );
}

export default App;
