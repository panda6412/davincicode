import { combineReducers } from 'redux'
import { daVinciCode } from '../features/daVinciCode/reducer'

export default combineReducers({
  daVinciCode
})
  